package com.jessecorbett.diskord.example

import com.jessecorbett.diskord.api.channel.Embed
import com.jessecorbett.diskord.api.channel.EmbedImage
import com.jessecorbett.diskord.api.common.Message
import com.jessecorbett.diskord.bot.bot
import com.jessecorbett.diskord.bot.classicCommands
import com.jessecorbett.diskord.bot.events
import com.jessecorbett.diskord.util.sendReply

val Message.words: List<String> get() {
    return content.split(" ")
}

suspend fun main() {
    bot("your-bot-token") {
        events {
            onMessageCreate { message ->
                if (message.content.contains("diskord")) {
                    message.react("💯")
                }
            }
        }

        classicCommands(".") { // "." is the default, but is provided here anyway for example purposes
            command("echo") { message ->
                message.reply(message.words.joinToString(" "))
            }

            // Like echo, but deletes the command message
            command("say") { message ->
                message.respond(message.words.joinToString(" "))
                message.delete()
            }

            command("cat") { message ->
                message.delete()

                channel(message.channelId).sendReply(message, embed = Embed(
                    description = "What a cute cat!",
                    image = EmbedImage(url = "https://placekitten.com/200/300", imageWidth = 200, imageHeight = 300)
                ))
            }
        }
    }
}
