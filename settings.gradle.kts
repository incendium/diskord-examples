rootProject.name = "diskord-examples"

include("jvm-commands", "jvm-ping-pong", "jvm-send-unprompted-message", "jvm-slash-commands", "jvm-split-logic")
