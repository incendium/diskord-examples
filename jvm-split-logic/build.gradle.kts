plugins {
    alias(libs.plugins.kotlin.jvm)
}

dependencies {
    implementation(libs.diskord.core)
    implementation(libs.diskord.bot)
}
